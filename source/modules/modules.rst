Модули сервера
==============

..  toctree::
    :maxdepth: 1

    country/country
    currency/currency
    address/address
    party/party
    product/product
    stock/stock
