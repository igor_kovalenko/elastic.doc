Модуль elastic.stock
====================

Модуль предназначен для учета количества и цен на товары предназначенные для продажи

Основная задача модуля - предоставить внятную информацию для остальных компонентов системы о том сколько и по какой цене
имеется товаров в наличии на каждом из мест храния, сколько товара находится в резерве, и сколько в пути

Для этих целей модуль предоставляет иерархический справочник мест хранения, документы движения и ценообразования товара,
а так же и регистры, отражающие общее состояние учетной системы в разрезах остатков, цен, резервов и товаров в пути



Документы модуля elastic.stock
------------------------------




Модели пакета
-------------

..  toctree::
    :maxdepth: 2

    models/stock_location
    models/consignment
    models/revaluation_act


REST API пакета
---------------

..  toctree::
    :maxdepth: 2

    api/uom_category-resource
    api/uom-resource
    api/product_category-resource
    api/product_group-resource
    api/product-resource
    api/product_additional_feature-resource

..  currentmodule:: elastic.stock
