Ресурс "Категория единиц измерения"
===================================

..  http:get:: /elastic/product/api/v1/product_category/(int:id)/

    Вернуть информацио о категории по ее идентификатору (`id`).

    **Пример запроса**:

    .. sourcecode:: http

        GET /elastic/product/api/v1/product_category/1/ HTTP/1.1
        Host: example.com
        Content-Type: application/json
        Accept: application/json

    **Пример ответа**:

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Vary: Accept
        Content-Type: application/json


        {
            "id": 1,
            "name": "Единицы длины"
        }

    :query filter: возможные параметры ``name``
    :query offset: смещение. По-умолчанию - 0
    :query limit: ограничить ответ количеством записей. По-умолчанию - 20
    :reqheader Content-Type: application/json
    :reqheader Accept: application/json или application/xml
    :resheader Content-Type: зависит от заголовка запроса `Accept`
    :statuscode 200: нет ошибок
    :statuscode 404: не верный id категории
