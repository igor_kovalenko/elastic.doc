Ресурс "Товарная группа"
========================

..  http:get:: /elastic/product/api/v1/product_group/(string:code)/

    Вернуть информацио о товарной группе по ее коду (`code`).

    **Пример запроса**:

    .. sourcecode:: http

        GET /elastic/product/api/v1/product_group/10603/ HTTP/1.1
        Host: example.com
        Content-Type: application/json
        Accept: application/json

    **Пример ответа**:

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Vary: Accept
        Content-Type: application/json

        {
            "code": "10603",
            "name": "Гигиена_расходники",
            "parent": {
                "code": "507",
                "name": "Гигиена",
                "parent": {
                    "code": "389",
                    "name": "Парафармация",
                    "parent": null
                }
            }
        }

    :query filter: возможные параметры ``name``, ``link_channels``, ``holidays``
    :query offset: смещение. По-умолчанию - 0
    :query limit: ограничить ответ количеством записей. По-умолчанию - 20
    :reqheader Content-Type: application/json
    :reqheader Accept: application/json или application/xml
    :resheader Content-Type: зависит от заголовка запроса `Accept`
    :statuscode 200: нет ошибок
    :statuscode 404: не верный id субъекта

..  http:post:: /elastic/product/api/v1/product_group/

    Создать новую товарную группу.

    **Пример запроса**:

    .. sourcecode:: http

        POST /elastic/product/api/v1/product_group/ HTTP/1.1
        Host: example.com
        Content-Type: application/json

        {
            "code": "10603",
            "name": "Гигиена_расходники",
            "parent": "507",
        }

    **Пример ответа**:

    .. sourcecode:: http

        HTTP/1.0 201 CREATED
        Date: Fri, 20 May 2011 06:48:36 GMT
        Server: WSGIServer/0.1 Python/2.7
        Content-Type: text/html; charset=utf-8
        Location: /elastic/product/api/v1/product_group/10603/
        Vary: *

    :reqheader Content-Type: application/json
    :statuscode 201: нет ошибок
    :statuscode 400: не верный запрос

..  http:put:: /elastic/product/api/v1/product_group/(string:code)/

    Обновить существующюю или создать новую товарную группу.

    **Пример запроса на обновление**:

    .. sourcecode:: http

        PUT /elastic/product/api/v1/product_group/10603/ HTTP/1.1
        Host: example.com
        Content-Type: application/json

        {
            "code": "10603",
            "name": "Гигиена и расходники",
            "parent": "507",
        }


    **Пример ответа**:

    .. sourcecode:: http

        HTTP/1.0 204 NO CONTENT
        Date: Fri, 20 May 2011 07:13:21 GMT
        Server: WSGIServer/0.1 Python/2.7
        Content-Length: 0
        Content-Type: text/html; charset=utf-8

    :reqheader Content-Type: application/json
    :statuscode 204: нет ошибок
    :statuscode 400: не верный запрос

..  http:patch:: /elastic/product/api/v1/product_group/(string:code)/

    Частичное обновление существующей товарной группы.

    **Пример запроса**:

    .. sourcecode:: http

        PATCH /elastic/product/api/v1/product_group/10603/ HTTP/1.1
        Host: example.com
        Content-Type: application/json

        {
            "name": "Гигиена, расходники",
        }

    **Пример ответа**:

    .. sourcecode:: http

        HTTP/1.0 202 ACCEPTED
        Date: Fri, 20 May 2011 07:13:21 GMT
        Server: WSGIServer/0.1 Python/2.7
        Content-Length: 0
        Content-Type: text/html; charset=utf-8

    :reqheader Content-Type: application/json
    :statuscode 202: нет ошибок
    :statuscode 400: не верный запрос

..  http:delete:: /elastic/product/api/v1/product_group/(string:code)/

    Удаление существующей товарной группы.
    **Внимание !** При удалении группы, все группы ссылающиеся на нее будут так же удалены рекурсивно.

    **Пример запроса**:

    .. sourcecode:: http

        DELETE /elastic/product/api/v1/product_group/10603/ HTTP/1.1
        Host: example.com

    **Пример ответа**:

    .. sourcecode:: http

        HTTP/1.0 204 NO CONTENT
        Date: Fri, 20 May 2011 07:28:01 GMT
        Server: WSGIServer/0.1 Python/2.7
        Content-Length: 0
        Content-Type: text/html; charset=utf-8

    :reqheader Content-Type: application/json
    :statuscode 202: нет ошибок
    :statuscode 400: не верный запрос