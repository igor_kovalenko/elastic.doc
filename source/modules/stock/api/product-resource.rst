Ресурс "Складская карточка (товар)"
===================================

..  http:get:: /elastic/product/api/v1/product/(string:sku)/

    Вернуть информацио о товара по его складскому идентификатору (`sku`).

    **Пример запроса**:

    .. sourcecode:: http

        GET /elastic/product/api/v1/product/00002/ HTTP/1.1
        Host: example.com
        Content-Type: application/json
        Accept: application/json

    **Пример ответа**:

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Vary: Accept
        Content-Type: application/json

        {
            "additional_features": [
                {
                    "id": 1,
                    "name": "Количество таблеток в упаковке",
                    "value": "20",
                    "uom": {
                        "category": {
                            "id": 7,
                            "name": "Экономические единицы"
                        },
                        "code": "796",
                        "form": "Международные",
                        "international_symbol": "pc; 1",
                        "name": "Штука",
                        "symbol": "шт"
                    },
                    "product": "/elastic/product/api/v1/product/00002/",
                }
            ],

            "category": {
                "code": "10",
                "name": "Складируемые"
            },

            "consignment_country": {
                "code": "643",
                "full_name": "Российская Федерация",
                "id": 186,
                "name": "РОССИЯ",
                "resource_uri": "/elastic/country/api/v1/country/643/",
                "symbol": "RU"
            },

            "description": null,

            "group": {
                "code": "46",
                "name": "Анальгетики",
                "parent": null
            },

            "manufacturer": "",
            "manufacturer_code": "",
            "name": "Пенталгин",
            "short_desc": "",
            "sku": "00002",

            "uom": {
                "category": {
                    "id": 7,
                    "name": "Экономические единицы"
                },
                "code": 796,
                "form": "Международные",
                "international_symbol": "pc; 1",
                "name": "Штука",
                "symbol": "шт"
            }
        }

    :query filter: возможные параметры ``name``, ``link_channels``, ``holidays``
    :query offset: смещение. По-умолчанию - 0
    :query limit: ограничить ответ количеством записей. По-умолчанию - 20
    :reqheader Content-Type: application/json
    :reqheader Accept: application/json или application/xml
    :resheader Content-Type: зависит от заголовка запроса `Accept`
    :statuscode 200: нет ошибок
    :statuscode 404: не верный id субъекта

..  http:post:: /elastic/product/api/v1/product/

    Создать новую дополнительную характеристику товара.

    **Пример запроса**:

    .. sourcecode:: http

        POST /elastic/product/api/v1/product/ HTTP/1.1
        Host: example.com
        Content-Type: application/json

        {
            "additional_features": [
                {
                    "name": "Количество таблеток в упаковке",
                    "value": "20",
                    "uom": "796"
                }
            ],

            "category": "10",
            "consignment_country": "643",
            "group": "46",
            "name": "Пенталгин",
            "sku": "00002",
            "uom": "796"
        }

    **Пример ответа**:

    .. sourcecode:: http

        HTTP/1.0 201 CREATED
        Date: Fri, 20 May 2011 06:48:36 GMT
        Server: WSGIServer/0.1 Python/2.7
        Content-Type: text/html; charset=utf-8
        Location: /elastic/product/api/v1/product/00002/
        Vary: *

    :reqheader Content-Type: application/json
    :statuscode 201: нет ошибок
    :statuscode 400: не верный запрос

..  http:put:: /elastic/product/api/v1/product/(string:sku)/

    Обновить существующюю или создать новую товарную группу.

    **Пример запроса на обновление**:

    .. sourcecode:: http

        PUT /elastic/product/api/v1/product/00002/ HTTP/1.1
        Host: example.com
        Content-Type: application/json

        {
            "additional_features": [
                {
                    "name": "Количество таблеток в упаковке",
                    "value": "28",
                    "uom": "796"
                }
            ],

            "category": "10",
            "consignment_country": "643",
            "group": "46",
            "name": "Пенталгин-М",
            "sku": "00002",
            "uom": "796"
        }


    **Пример ответа**:

    .. sourcecode:: http

        HTTP/1.0 204 NO CONTENT
        Date: Fri, 20 May 2011 07:13:21 GMT
        Server: WSGIServer/0.1 Python/2.7
        Content-Length: 0
        Content-Type: text/html; charset=utf-8

    :reqheader Content-Type: application/json
    :statuscode 204: нет ошибок
    :statuscode 400: не верный запрос

..  http:patch:: /elastic/product/api/v1/product/(string:sku)/

    Частичное обновление существующей товарной группы.

    **Пример запроса**:

    .. sourcecode:: http

        PATCH /elastic/product/api/v1/product/00002/ HTTP/1.1
        Host: example.com
        Content-Type: application/json

        {
            "name": "Пенталгин-М",
        }

    **Пример ответа**:

    .. sourcecode:: http

        HTTP/1.0 202 ACCEPTED
        Date: Fri, 20 May 2011 07:13:21 GMT
        Server: WSGIServer/0.1 Python/2.7
        Content-Length: 0
        Content-Type: text/html; charset=utf-8

    :reqheader Content-Type: application/json
    :statuscode 202: нет ошибок
    :statuscode 400: не верный запрос

..  http:delete:: /elastic/product/api/v1/product/(string:sku)/

    Удаление существующей товарной группы.

    **Пример запроса**:

    .. sourcecode:: http

        DELETE /elastic/product/api/v1/product/00002/ HTTP/1.1
        Host: example.com

    **Пример ответа**:

    .. sourcecode:: http

        HTTP/1.0 204 NO CONTENT
        Date: Fri, 20 May 2011 07:28:01 GMT
        Server: WSGIServer/0.1 Python/2.7
        Content-Length: 0
        Content-Type: text/html; charset=utf-8

    :reqheader Content-Type: application/json
    :statuscode 202: нет ошибок
    :statuscode 400: не верный запрос