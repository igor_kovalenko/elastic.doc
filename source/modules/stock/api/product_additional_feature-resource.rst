Ресурс "Дополнительные характеристики товара"
=============================================

..  http:get:: /elastic/product/api/v1/product_feature/(int:id)/

    Вернуть информацио о дополнительной характеристике товара по ее идентификатору (`id`).

    **Пример запроса**:

    .. sourcecode:: http

        GET /elastic/product/api/v1/product_feature/1/ HTTP/1.1
        Host: example.com
        Content-Type: application/json
        Accept: application/json

    **Пример ответа**:

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Vary: Accept
        Content-Type: application/json

        {
            "id": 1,
            "name": "Количество таблеток в упаковке",
            "value": "20",
            "uom": {
                "category": {
                    "id": 7,
                    "name": "Экономические единицы"
                },
                "code": "796",
                "form": "Международные",
                "international_symbol": "pc; 1",
                "name": "Штука",
                "symbol": "шт"
            },
            "product": "/elastic/product/api/v1/product/00001/",
        }

    :query filter: возможные параметры ``name``, ``value``
    :query offset: смещение. По-умолчанию - 0
    :query limit: ограничить ответ количеством записей. По-умолчанию - 20
    :reqheader Content-Type: application/json
    :reqheader Accept: application/json или application/xml
    :resheader Content-Type: зависит от заголовка запроса `Accept`
    :statuscode 200: нет ошибок
    :statuscode 404: не верный id субъекта

..  http:post:: /elastic/product/api/v1/product_feature/

    Создать новую дополнительную характеристику товара.

    **Пример запроса**:

    .. sourcecode:: http

        POST /elastic/product/api/v1/product_feature/ HTTP/1.1
        Host: example.com
        Content-Type: application/json

        {
            "name": "Количество таблеток в упаковке",
            "value": "20",
            "uom": "796",
            "product": "/elastic/product/api/v1/product/00001/",
        }

    **Пример ответа**:

    .. sourcecode:: http

        HTTP/1.0 201 CREATED
        Date: Fri, 20 May 2011 06:48:36 GMT
        Server: WSGIServer/0.1 Python/2.7
        Content-Type: text/html; charset=utf-8
        Location: /elastic/product/api/v1/product_feature/1/
        Vary: *

    :reqheader Content-Type: application/json
    :statuscode 201: нет ошибок
    :statuscode 400: не верный запрос

..  http:put:: /elastic/product/api/v1/product_feature/(int:id)/

    Обновить существующую или создать новую дополнительную характеристику товара.

    **Пример запроса на обновление**:

    .. sourcecode:: http

        PUT /elastic/product/api/v1/product_feature/1/ HTTP/1.1
        Host: example.com
        Content-Type: application/json

        {
            "name": "Количество таблеток в упаковке",
            "value": "25",
            "uom": "796",
            "product": "/elastic/product/api/v1/product/00001/",
        }


    **Пример ответа**:

    .. sourcecode:: http

        HTTP/1.0 204 NO CONTENT
        Date: Fri, 20 May 2011 07:13:21 GMT
        Server: WSGIServer/0.1 Python/2.7
        Content-Length: 0
        Content-Type: text/html; charset=utf-8

    :reqheader Content-Type: application/json
    :statuscode 204: нет ошибок
    :statuscode 400: не верный запрос

..  http:patch:: /elastic/product/api/v1/product_feature/(int:id)/

    Частичное обновление существующей дополнительной характеристики товара.

    **Пример запроса**:

    .. sourcecode:: http

        PATCH /elastic/product/api/v1/product_feature/1/ HTTP/1.1
        Host: example.com
        Content-Type: application/json

        {
            "value": "25",
        }

    **Пример ответа**:

    .. sourcecode:: http

        HTTP/1.0 202 ACCEPTED
        Date: Fri, 20 May 2011 07:13:21 GMT
        Server: WSGIServer/0.1 Python/2.7
        Content-Length: 0
        Content-Type: text/html; charset=utf-8

    :reqheader Content-Type: application/json
    :statuscode 202: нет ошибок
    :statuscode 400: не верный запрос

..  http:delete:: /elastic/product/api/v1/product_feature/(int:id)/

    Удаление существующей дополнительной характеристики товара.

    **Пример запроса**:

    .. sourcecode:: http

        DELETE /elastic/product/api/v1/product_feature/1/ HTTP/1.1
        Host: example.com

    **Пример ответа**:

    .. sourcecode:: http

        HTTP/1.0 204 NO CONTENT
        Date: Fri, 20 May 2011 07:28:01 GMT
        Server: WSGIServer/0.1 Python/2.7
        Content-Length: 0
        Content-Type: text/html; charset=utf-8

    :reqheader Content-Type: application/json
    :statuscode 202: нет ошибок
    :statuscode 400: не верный запрос