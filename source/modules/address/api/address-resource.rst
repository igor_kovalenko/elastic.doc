Ресурс "Почтовый адрес"
=======================

..  http:get:: /elastic/address/api/v1/address/(int:id)/

    Вернуть информацио об адресе по его идентификатору (`id`).

    **Пример запроса**:

    .. sourcecode:: http

        GET /elastic/address/api/v1/address/1/ HTTP/1.1
        Host: example.com
        Content-Type: application/json
        Accept: application/json

    **Пример ответа**:

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Vary: Accept
        Content-Type: application/json

        {
            "address_string": "Российская Федерация - 690000, Владивосток, Фонтанная 2",
            "city": "Владивосток",
            "country": {
                "code": "643",
                "full_name": "Российская Федерация",
                "id": 186,
                "name": "РОССИЯ",
                "resource_uri": "/elastic/country/api/v1/country/643/",
                "symbol": "RU"
            },
            "district": "",
            "flat": "",
            "house": "2",
            "id": 1,
            "postal_index": "690000",
            "street": "Фонтанная"
        }

    :query filter: возможные параметры ``country``, ``city``, ``postal_index``
    :query offset: смещение. По-умолчанию - 0
    :query limit: ограничить ответ количеством записей. По-умолчанию - 20
    :reqheader Content-Type: application/json
    :reqheader Accept: application/json или application/xml
    :resheader Content-Type: зависит от заголовка запроса `Accept`
    :statuscode 200: нет ошибок
    :statuscode 404: не верный id адреса

..  http:post:: /elastic/address/api/v1/address/

    Создать новый почтовый адрес.

    **Пример запроса**:

    .. sourcecode:: http

        POST /elastic/address/api/v1/address/ HTTP/1.1
        Host: example.com
        Content-Type: application/json

        {
            "city": "Владивосток",
            "country": "643",
            "house": "2",
            "postal_index": "690000",
            "street": "Фонтанная"
        }

    **Пример ответа**:

    .. sourcecode:: http

        HTTP/1.0 201 CREATED
        Date: Fri, 20 May 2011 06:48:36 GMT
        Server: WSGIServer/0.1 Python/2.7
        Content-Type: text/html; charset=utf-8
        Location: /elastic/address/api/v1/address/1/
        Vary: *

    :reqheader Content-Type: application/json
    :statuscode 201: нет ошибок
    :statuscode 400: не верный запрос

..  http:put:: /elastic/address/api/v1/address/(int:id)/

    Обновить существующий или создать новый почтовый адрес.

    **Пример запроса на обновление**:

    .. sourcecode:: http

        PUT /elastic/address/api/v1/address/1/ HTTP/1.1
        Host: example.com
        Content-Type: application/json

        {
            "city": "Владивосток",
            "country": "643",
            "house": "2а",
            "postal_index": "690000",
            "street": "Фонтанная"
        }

    **Пример ответа**:

    .. sourcecode:: http

        HTTP/1.0 204 NO CONTENT
        Date: Fri, 20 May 2011 07:13:21 GMT
        Server: WSGIServer/0.1 Python/2.7
        Content-Length: 0
        Content-Type: text/html; charset=utf-8

    :reqheader Content-Type: application/json
    :statuscode 204: нет ошибок
    :statuscode 400: не верный запрос

..  http:patch:: /elastic/address/api/v1/address/(int:id)/

    Частичное обновление существующего почтового адреса.

    **Пример запроса**:

    .. sourcecode:: http

        PATCH /elastic/address/api/v1/address/1/ HTTP/1.1
        Host: example.com
        Content-Type: application/json

        {
            "house": "2б",
        }

    **Пример ответа**:

    .. sourcecode:: http

        HTTP/1.0 202 ACCEPTED
        Date: Fri, 20 May 2011 07:13:21 GMT
        Server: WSGIServer/0.1 Python/2.7
        Content-Length: 0
        Content-Type: text/html; charset=utf-8

    :reqheader Content-Type: application/json
    :statuscode 202: нет ошибок
    :statuscode 400: не верный запрос

..  http:delete:: /elastic/address/api/v1/address/(int:id)/

    Удаление существующего почтового адреса.

    **Пример запроса**:

    .. sourcecode:: http

        DELETE /elastic/address/api/v1/address/1/ HTTP/1.1
        Host: example.com

    **Пример ответа**:

    .. sourcecode:: http

        HTTP/1.0 204 NO CONTENT
        Date: Fri, 20 May 2011 07:28:01 GMT
        Server: WSGIServer/0.1 Python/2.7
        Content-Length: 0
        Content-Type: text/html; charset=utf-8

    :reqheader Content-Type: application/json
    :statuscode 202: нет ошибок
    :statuscode 400: не верный запрос
