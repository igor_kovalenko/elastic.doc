Ресурс "Страны мира"
====================

..  http:get:: /elastic/country/api/v1/country/(int:code)/

    Вернуть информацио о стране по ее коду (`code`).

    **Пример запроса**:

    .. code-block:: HTTP

        GET /elastic/country/api/v1/country/643/ HTTP/1.1
        Host: example.com
        Content-Type: application/json
        Accept: application/json

    **Пример ответа**:

    .. code-block:: HTTP

        HTTP/1.1 200 OK
        Vary: Accept
        Content-Type: application/json

        {
            "code": "643",
            "full_name": "Российская Федерация",
            "id": 186,
            "name": "РОССИЯ",
            "resource_uri": "/elastic/country/api/v1/country/643/",
            "symbol": "RU"
        }

    :query filter: возможные параметры ``name``, ``symbol``, ``code``
    :query offset: смещение. По-умолчанию - 0
    :query limit: ограничить ответ количеством записей. По-умолчанию - 20
    :reqheader Content-Type: application/json
    :reqheader Accept: application/json или application/xml
    :resheader Content-Type: зависит от заголовка запроса `Accept`
    :statuscode 200: нет ошибок
    :statuscode 404: не верный код страны