Ресурс "Валюты"
===============

..  http:get:: /elastic/currency/api/v1/currency/(int:code)/

    Вернуть информацио о валюте по ее коду (`code`).

    **Пример запроса**:

    .. sourcecode:: http

        GET /elastic/currency/api/v1/currency/643/ HTTP/1.1
        Host: example.com
        Content-Type: application/json
        Accept: application/json

    **Пример ответа**:

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Vary: Accept
        Content-Type: application/json

        {
            "code": 643,
            "id": 1,
            "name": "Российский рубль",
            "resource_uri": "/elastic/currency/api/v1/currency/643/",
            "symbol": "RUB"
        }

    :query filter: возможные параметры ``name``, ``symbol``, ``code``
    :query offset: смещение. По-умолчанию - 0
    :query limit: ограничить ответ количеством записей. По-умолчанию - 20
    :reqheader Content-Type: application/json
    :reqheader Accept: application/json или application/xml
    :resheader Content-Type: зависит от заголовка запроса `Accept`
    :statuscode 200: нет ошибок
    :statuscode 404: не верный код валюты
