Справочник "Валюты"
===================

Справочник не реплицируется.

Модель "Валюты"
---------------

..  autoclass:: elastic.currency.models.Currency
    :members:


Моедль "Курсы валюты"
---------------------

..  autoclass:: elastic.currency.models.Rate
    :members:
