Ресурс "Единица измерения"
==========================

..  http:get:: /elastic/product/api/v1/uom/(string:code)/

    Вернуть информацио о единице измерения по ее коду (`code`).

    **Пример запроса**:

    .. sourcecode:: http

        GET /elastic/product/api/v1/uom/6/ HTTP/1.1
        Host: example.com
        Content-Type: application/json
        Accept: application/json

    **Пример ответа**:

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Vary: Accept
        Content-Type: application/json

        {
            "category": {
                "id": 1,
                "name": "Единицы длины"
            },
            "code": 6,
            "form": "Международные",
            "international_symbol": "m",
            "name": "Метр",
            "symbol": "м"
        }

    :query filter: возможные параметры ``name``, ``symbol``, ``category``
    :query offset: смещение. По-умолчанию - 0
    :query limit: ограничить ответ количеством записей. По-умолчанию - 20
    :reqheader Content-Type: application/json
    :reqheader Accept: application/json или application/xml
    :resheader Content-Type: зависит от заголовка запроса `Accept`
    :statuscode 200: нет ошибок
    :statuscode 404: не верный код единицы измерения
