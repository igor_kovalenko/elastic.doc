Ресурс "Категория товара"
=========================

..  http:get:: /elastic/product/api/v1/product_category/(string:code)/

    Вернуть информацио о категории товара по ее коду (`code`).

    **Пример запроса**:

    .. sourcecode:: http

        GET /elastic/product/api/v1/product_category/10/ HTTP/1.1
        Host: example.com
        Content-Type: application/json
        Accept: application/json

    **Пример ответа**:

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Vary: Accept
        Content-Type: application/json

        {
            "code": "10",
            "name": "Складируемые"
        }

    :query filter: возможные параметры ``name``, ``code``
    :query offset: смещение. По-умолчанию - 0
    :query limit: ограничить ответ количеством записей. По-умолчанию - 20
    :reqheader Content-Type: application/json
    :reqheader Accept: application/json или application/xml
    :resheader Content-Type: зависит от заголовка запроса `Accept`
    :statuscode 200: нет ошибок
    :statuscode 404: не верный id субъекта
