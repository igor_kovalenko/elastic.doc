Спарвочник "Складские карточки (товары)"
========================================

Справочник складских карточочек. Данный справочник является реплицируемым.

Модель "Складская карточка"
---------------------------

..  autoclass:: elastic.product.models.Product
    :members:


Модель "Группа товара"
----------------------

..  autoclass:: elastic.product.models.ProductGroup
    :members:


Модель "Категория товара"
-------------------------

..  autoclass:: elastic.product.models.ProductCategory
    :members:

Модель "Дополнительные характеристики товара"
---------------------------------------------

..  autoclass:: elastic.product.models.ProductAdditionalFeature
    :members:
