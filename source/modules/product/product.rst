Модуль elastic.product
======================

Модуль реализует справочник "Складские карточки" и `общероссийский классификатор единиц измерения <https://ru.wikipedia.org/wiki/%D0%9E%D0%B1%D1%89%D0%B5%D1%80%D0%BE%D1%81%D1%81%D0%B8%D0%B9%D1%81%D0%BA%D0%B8%D0%B9_%D0%BA%D0%BB%D0%B0%D1%81%D1%81%D0%B8%D1%84%D0%B8%D0%BA%D0%B0%D1%82%D0%BE%D1%80_%D0%B5%D0%B4%D0%B8%D0%BD%D0%B8%D1%86_%D0%B8%D0%B7%D0%BC%D0%B5%D1%80%D0%B5%D0%BD%D0%B8%D1%8F>`_


Справочники модуля
------------------

..  toctree::
    :maxdepth: 2

    references/uom
    references/product

REST API пакета
---------------

..  toctree::
    :maxdepth: 2

    api/uom_category-resource
    api/uom-resource
    api/product_category-resource
    api/product_group-resource
    api/product-resource
    api/product_additional_feature-resource

..  currentmodule:: elastic.product
