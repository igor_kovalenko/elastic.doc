Ресурс "Канал связи"
====================

..  http:get:: /elastic/party/api/v1/link_channel/(int:id)/

    Вернуть информацио о канале связи по его идентификатору (`id`).

    **Пример запроса**:

    .. sourcecode:: http

        GET /elastic/party/api/v1/link_channel/1/ HTTP/1.1
        Host: example.com
        Content-Type: application/json
        Accept: application/json

    **Пример ответа**:

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Vary: Accept
        Content-Type: application/json

        {
            "id": 1,
            "is_verified": false,
            "name": "Phone",
            "party": "/elastic/party/api/v1/party/1/",
            "value": "+7 (423) 2222-666"
        }

    :query filter: возможные параметры ``name``, ``holiday``, ``party``
    :query offset: смещение. По-умолчанию - 0
    :query limit: ограничить ответ количеством записей. По-умолчанию - 20
    :reqheader Content-Type: application/json
    :reqheader Accept: application/json или application/xml
    :resheader Content-Type: зависит от заголовка запроса `Accept`
    :statuscode 200: нет ошибок
    :statuscode 404: не верный id адреса

..  http:post:: /elastic/party/api/v1/link_channel/

    Создать новый канал связи.

    **Пример запроса**:

    .. sourcecode:: http

        POST /elastic/party/api/v1/link_channel/ HTTP/1.1
        Host: example.com
        Content-Type: application/json

        {
            "is_verified": false,
            "name": "Phone",
            "party": "/elastic/party/api/v1/party/1/",
            "value": "+7 (423) 2222-666"
        }

    **Пример ответа**:

    .. sourcecode:: http

        HTTP/1.0 201 CREATED
        Date: Fri, 20 May 2011 06:48:36 GMT
        Server: WSGIServer/0.1 Python/2.7
        Content-Type: text/html; charset=utf-8
        Location: /elastic/party/api/v1/link_channel/1/
        Vary: *

    :reqheader Content-Type: application/json
    :statuscode 201: нет ошибок
    :statuscode 400: не верный запрос

..  http:put:: /elastic/party/api/v1/link_channel/(int:id)/

    Обновить существующий или создать новый канал связи.

    **Пример запроса на обновление**:

    .. sourcecode:: http

        PUT /elastic/party/api/v1/link_channel/1/ HTTP/1.1
        Host: example.com
        Content-Type: application/json

        {
            "is_verified": true,
            "name": "Phone",
            "party": "/elastic/party/api/v1/party/1/",
            "value": "+7 (423) 2222-666"
        }

    **Пример ответа**:

    .. sourcecode:: http

        HTTP/1.0 204 NO CONTENT
        Date: Fri, 20 May 2011 07:13:21 GMT
        Server: WSGIServer/0.1 Python/2.7
        Content-Length: 0
        Content-Type: text/html; charset=utf-8

    :reqheader Content-Type: application/json
    :statuscode 204: нет ошибок
    :statuscode 400: не верный запрос

..  http:patch:: /elastic/party/api/v1/link_channel/(int:id)/

    Частичное обновление существующего канала связи.

    **Пример запроса**:

    .. sourcecode:: http

        PATCH /elastic/party/api/v1/link_channel/1/ HTTP/1.1
        Host: example.com
        Content-Type: application/json

        {
            "is_verified": true
        }

    **Пример ответа**:

    .. sourcecode:: http

        HTTP/1.0 202 ACCEPTED
        Date: Fri, 20 May 2011 07:13:21 GMT
        Server: WSGIServer/0.1 Python/2.7
        Content-Length: 0
        Content-Type: text/html; charset=utf-8

    :reqheader Content-Type: application/json
    :statuscode 202: нет ошибок
    :statuscode 400: не верный запрос

..  http:delete:: /elastic/party/api/v1/link_channel/(int:id)/

    Удаление существующего почтового канала связи.

    **Пример запроса**:

    .. sourcecode:: http

        DELETE /elastic/party/api/v1/link_channel/1/ HTTP/1.1
        Host: example.com

    **Пример ответа**:

    .. sourcecode:: http

        HTTP/1.0 204 NO CONTENT
        Date: Fri, 20 May 2011 07:28:01 GMT
        Server: WSGIServer/0.1 Python/2.7
        Content-Length: 0
        Content-Type: text/html; charset=utf-8

    :reqheader Content-Type: application/json
    :statuscode 202: нет ошибок
    :statuscode 400: не верный запрос