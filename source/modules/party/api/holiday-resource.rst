Ресурс "Праздники"
==================

..  http:get:: /elastic/party/api/v1/holiday/(int:id)/

    Вернуть информацио о празднике по его идентификатору (`id`).

    **Пример запроса**:

    .. sourcecode:: http

        GET /elastic/party/api/v1/holiday/1/ HTTP/1.1
        Host: example.com
        Content-Type: application/json
        Accept: application/json

    **Пример ответа**:

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Vary: Accept
        Content-Type: application/json

        {
            "holiday": "1972-08-07",
            "id": 1,
            "name": "День рождения",
            "party": "/elastic/party/api/v1/party/2/"
        }

    :query filter: возможные параметры ``name``, ``holiday``, ``party``
    :query offset: смещение. По-умолчанию - 0
    :query limit: ограничить ответ количеством записей. По-умолчанию - 20
    :reqheader Content-Type: application/json
    :reqheader Accept: application/json или application/xml
    :resheader Content-Type: зависит от заголовка запроса `Accept`
    :statuscode 200: нет ошибок
    :statuscode 404: не верный id адреса

..  http:post:: /elastic/party/api/v1/holiday/ HTTP/1.1

    Создать новый праздник.

    **Пример запроса**:

    .. sourcecode:: http

        POST /elastic/party/api/v1/holiday/ HTTP/1.1
        Host: example.com
        Content-Type: application/json

        {
            "holiday": "1972-08-07",
            "name": "День рождения",
            "party": "/elastic/party/api/v1/party/2/"
        }

    **Пример ответа**:

    .. sourcecode:: http

        HTTP/1.0 201 CREATED
        Date: Fri, 20 May 2011 06:48:36 GMT
        Server: WSGIServer/0.1 Python/2.7
        Content-Type: text/html; charset=utf-8
        Location: /elastic/party/api/v1/holiday/1/
        Vary: *

    :reqheader Content-Type: application/json
    :statuscode 201: нет ошибок
    :statuscode 400: не верный запрос

..  http:put:: /elastic/party/api/v1/holiday/(int:id)/

    Обновить существующий или создать новый праздник.

    **Пример запроса на обновление**:

    .. sourcecode:: http

        PUT /elastic/party/api/v1/holiday/1/ HTTP/1.1
        Host: example.com
        Content-Type: application/json

        {
            "holiday": "1972-08-07",
            "name": "День варенья",
            "party": "/elastic/party/api/v1/party/2/"
        }

    **Пример ответа**:

    .. sourcecode:: http

        HTTP/1.0 204 NO CONTENT
        Date: Fri, 20 May 2011 07:13:21 GMT
        Server: WSGIServer/0.1 Python/2.7
        Content-Length: 0
        Content-Type: text/html; charset=utf-8

    :reqheader Content-Type: application/json
    :statuscode 204: нет ошибок
    :statuscode 400: не верный запрос

..  http:patch:: /elastic/party/api/v1/holiday/(int:id)/

    Частичное обновление существующего праздника.

    **Пример запроса**:

    .. sourcecode:: http

        PATCH /elastic/party/api/v1/holiday/1/ HTTP/1.1
        Host: example.com
        Content-Type: application/json

        {
            "name": "День варенья"
        }

    **Пример ответа**:

    .. sourcecode:: http

        HTTP/1.0 202 ACCEPTED
        Date: Fri, 20 May 2011 07:13:21 GMT
        Server: WSGIServer/0.1 Python/2.7
        Content-Length: 0
        Content-Type: text/html; charset=utf-8

    :reqheader Content-Type: application/json
    :statuscode 202: нет ошибок
    :statuscode 400: не верный запрос

..  http:delete:: /elastic/party/api/v1/holiday/(int:id)/

    Удаление существующего почтового адреса.

    **Пример запроса**:

    .. sourcecode:: http

        DELETE /elastic/party/api/v1/holiday/1/ HTTP/1.1
        Host: example.com

    **Пример ответа**:

    .. sourcecode:: http

        HTTP/1.0 204 NO CONTENT
        Date: Fri, 20 May 2011 07:28:01 GMT
        Server: WSGIServer/0.1 Python/2.7
        Content-Length: 0
        Content-Type: text/html; charset=utf-8

    :reqheader Content-Type: application/json
    :statuscode 202: нет ошибок
    :statuscode 400: не верный запрос