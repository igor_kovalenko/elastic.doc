Ресурс "Субъект"
================

..  http:get:: /elastic/party/api/v1/party/(int:id)/

    Вернуть информацио о субъекте по его идентификатору (`id`).

    **Пример запроса**:

    ..  sourcecode:: http

        GET /elastic/party/api/v1/party/1/ HTTP/1.1
        Host: example.com
        Content-Type: application/json
        Accept: application/json

    **Пример ответа**:

    ..  sourcecode:: http

        HTTP/1.1 200 OK
        Vary: Accept
        Content-Type: application/json

        {
            "addresses": [
                {
                    "address_string": "Российская Федерация - 690000, Владивосток, Фонтанная 2",
                    "city": "Владивосток",
                    "country": {
                        "code": "643",
                        "full_name": "Российская Федерация",
                        "id": 186,
                        "name": "РОССИЯ",
                        "resource_uri": "/elastic/country/api/v1/country/643/",
                        "symbol": "RU"
                    },
                    "district": "",
                    "flat": "",
                    "house": "2",
                    "id": 1,
                    "postal_index": "690000",
                    "street": "Фонтанная"
                }
            ],
            "first_name": "",
            "gender": "undefined",
            "holidays": [ ],
            "id": 1,
            "last_name": "",
            "link_channels": [
                {
                    "id": 1,
                    "is_verified": false,
                    "name": "Phone",
                    "party": "/elastic/party/api/v1/party/1/",
                    "value": "+7 (423) 2222-666"
                }
            ],
            "marital_status": "undefined",
            "middle_name": "",
            "name": "Ресторан 'Зума'"
        }

    :query filter: возможные параметры ``name``, ``link_channels``, ``holidays``
    :query offset: смещение. По-умолчанию - 0
    :query limit: ограничить ответ количеством записей. По-умолчанию - 20
    :reqheader Content-Type: application/json
    :reqheader Accept: application/json или application/xml
    :resheader Content-Type: зависит от заголовка запроса `Accept`
    :statuscode 200: нет ошибок
    :statuscode 404: не верный id субъекта

..  http:post:: /elastic/party/api/v1/party/

    Создать нового субъекта.

    **Пример запроса**:

    ..  sourcecode:: http

        POST /elastic/party/api/v1/party/ HTTP/1.1
        Host: example.com
        Content-Type: application/json

        {
            "addresses": [
                {
                    "address_string": "Российская Федерация - 690000, Владивосток, Фонтанная 2",
                    "city": "Владивосток",
                    "country": "643",
                    "house": "2",
                    "postal_index": "690000",
                    "street": "Фонтанная"
                }
            ],
            "link_channels": [
                {
                    "name": "Phone",
                    "value": "+7 (423) 2222-666"
                }
            ],
            "name": "Ресторан  'Зума'"
        }

    **Пример ответа**:

    ..  sourcecode:: http

        HTTP/1.0 201 CREATED
        Date: Fri, 20 May 2011 06:48:36 GMT
        Server: WSGIServer/0.1 Python/2.7
        Content-Type: text/html; charset=utf-8
        Location: /elastic/party/api/v1/party/1/
        Vary: *

    :reqheader Content-Type: application/json
    :statuscode 201: нет ошибок
    :statuscode 400: не верный запрос

..  http:put:: /elastic/party/api/v1/party/(int:id)/

    Обновить существующий или создать нового субъекта.

    **Пример запроса на обновление**:

    ..  sourcecode:: http

        PUT /elastic/party/api/v1/party/1/ HTTP/1.1
        Host: example.com
        Content-Type: application/json

        {
            "addresses": [
                {
                    "address_string": "Российская Федерация - 690000, Владивосток, Фонтанная 2",
                    "city": "Владивосток",
                    "country": "643",
                    "house": "3а",
                    "postal_index": "690000",
                    "street": "Фонтанная"
                }
            ],
            "link_channels": [
                {
                    "name": "Phone",
                    "value": "+7 (423) 2222-666"
                }
            ],
            "name": "Паназиатский ресторан 'Зума'"
        }

    **Пример ответа**:

    ..  sourcecode:: http

        HTTP/1.0 204 NO CONTENT
        Date: Fri, 20 May 2011 07:13:21 GMT
        Server: WSGIServer/0.1 Python/2.7
        Content-Length: 0
        Content-Type: text/html; charset=utf-8

    :reqheader Content-Type: application/json
    :statuscode 204: нет ошибок
    :statuscode 400: не верный запрос

..  http:patch:: /elastic/party/api/v1/party/(int:id)/

    Частичное обновление существующего субъекта.

    **Пример запроса**:

    ..  sourcecode:: http

        PATCH /elastic/party/api/v1/party/1/ HTTP/1.1
        Host: example.com
        Content-Type: application/json

        {
            "name": "Паназиатский ресторан 'Зума'"
        }

    **Пример ответа**:

    .. sourcecode:: http

        HTTP/1.0 202 ACCEPTED
        Date: Fri, 20 May 2011 07:13:21 GMT
        Server: WSGIServer/0.1 Python/2.7
        Content-Length: 0
        Content-Type: text/html; charset=utf-8

    :reqheader Content-Type: application/json
    :statuscode 202: нет ошибок
    :statuscode 400: не верный запрос

..  http:delete:: /elastic/party/api/v1/party/(int:id)/

    Удаление существующего субъекта.

    **Пример запроса**:

    ..  sourcecode:: http

        DELETE /elastic/party/api/v1/party/1/ HTTP/1.1
        Host: example.com

    **Пример ответа**:

    ..  sourcecode:: http

        HTTP/1.0 204 NO CONTENT
        Date: Fri, 20 May 2011 07:28:01 GMT
        Server: WSGIServer/0.1 Python/2.7
        Content-Length: 0
        Content-Type: text/html; charset=utf-8

    :reqheader Content-Type: application/json
    :statuscode 202: нет ошибок
    :statuscode 400: не верный запрос