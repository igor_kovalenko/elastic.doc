Справочник "Субъекты"
=====================

Модель "Субъект"
----------------

..  autoclass:: elastic.party.models.Party
    :members:


Модель "Канал связи"
--------------------

Содержит каналы связи с субъектом, такие как телефоны, адреса электронной почты и т. д.

..  autoclass:: elastic.party.models.LinkChannel
    :members:


Модель "Памятная дата"
----------------------

Содержит памятные даты (праздники) субъекта.

..  autoclass:: elastic.party.models.Holiday
    :members:


Модель "Фотография"
-------------------

Содержит фотографии субъекта.

..  autoclass:: elastic.party.models.Photo
    :members:
