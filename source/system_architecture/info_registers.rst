Регистры сведений
=================

Регистры сведений или просто Регистры - это основной механизм позволяющий судить о состоянии сервера.

Регистры определяются в модулях сервера. К примеру, модуль Stock дает определение регистра остатков,
позволяющие судить о текущих товарных запасах

**Любой регистр содержит:**

* некий произвольный набор значений (сведения)
* дату на которую сведения актуальны
* строки табличной части, каждая из которых является, по сути, протоколом (логом) атомарной операции над регистром

**Строки регистра всегда содержат:**

* уникальный номер (GUID) проведенной операции над регистром
* дату проведения операции
* дату вступления в силу документа-основания
* уникальный внутренний код (GUID) документа-основания
* какие-то произвольные данные, позволяющие установить новые значения регистра

Особенно важен атрибут даты вступления документа-основания в силу, поскольку по этой дате определяется очередность
операций. К примеру, регистр цен определяет текущую цену на товар, как цену с самой старшей датой вступления в силу
документа-основания; при этом дата проведения самой операции особого значения не имеет.

Сервер реплицирует данные регистров с исходного на прочие узлы кластера без репликации документов-оснований.
Это очень важно в решениях, где часть узлов сервера является публичной и не должна хранить исходные документы.
