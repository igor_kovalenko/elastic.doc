Установка сервера
=================

Устанавливаем сервер. Сервер устанавливается с помощью команды pip

``pip install elastic-trade-server.meta``


Вносим изменения в конфигурационный файл settings.py


.. code-block:: python

    MEDIA_URL = '/media/'
    MEDIA_ROOT = os.path.join(BASE_DIR, "media")

    THUMBNAIL_PROCESSORS = (
        'easy_thumbnails.processors.colorspace',
        'easy_thumbnails.processors.autocrop',
        #'easy_thumbnails.processors.scale_and_crop',
        'filer.thumbnail_processors.scale_and_crop_with_subject_location',
        'easy_thumbnails.processors.filters',
    )

    THUMBNAIL_HIGH_RESOLUTION = True

    INSTALLED_APPS += (
        'autocomplete_light',
        'mptt',
        'mpttadmin',
        'easy_thumbnails',
        'filer',
        'ckeditor',
        'django_ace',
        'taggit',
        'elastic.currency.apps.ElasticCurrency',
        'elastic.country.apps.ElasticCountry',
        'elastic.address.apps.ElasticAddress',
        'elastic.party.apps.ElasticParty',
        'elastic.workflow.apps.ElasticWorkflow',
        'elastic.product.apps.ElasticProduct',
        'elastic.stock.apps.ElasticStock',
        'elastic.organization.apps.ElasticOrganization',
        'elastic.delivery.apps.ElasticDelivery',
        'elastic.articles.apps.ElasticArticle',
        'elastic.product_catalog.apps.ElasticProductCatalog',
    )

    CKEDITOR_CONFIGS = {
        'default': {
            'toolbar': 'CMS',
            'height': 300,
            'width': 700,
        },
    }

    CKEDITOR_MEDIA_PREFIX = os.path.join(MEDIA_ROOT, 'ckeditor')

    CKEDITOR_UPLOAD_PATH = "uploads/"

    ELASTIC_SETTINGS = {
        'server_name': "<уникальное имя нода>",
    }



Добавляем следующие строчки в urls.py

.. code-block:: python

    urlpatterns += [
        url(r'^ckeditor/', include('ckeditor.urls')),
        url(r'^autocomplete/', include('autocomplete_light.urls')),
    ]



Для тестов так же можно добавить и такие строки:

.. code-block:: python

    urlpatterns += [
        url(r'^elastic/currency/', include('elastic.currency.urls')),
        url(r'^elastic/country/', include('elastic.country.urls')),
        url(r'^elastic/address/', include('elastic.address.urls')),
        url(r'^elastic/party/', include('elastic.party.urls')),
        url(r'^elastic/product/', include('elastic.product.urls')),
        url(r'^elastic/stock/', include('elastic.stock.urls')),
        url(r'^elastic/organization/', include('elastic.organization.urls')),
        url(r'^elastic/delivery/', include('elastic.delivery.urls')),
        url(r'^elastic/product_catalog/', include('elastic.product_catalog.urls')),
    ]


Применяем миграции командой

``python manage.py migrate``


Сервер готов к работе!
